import requests
from urllib.parse import urlparse
from bs4 import BeautifulSoup

# CRAWLER #
def crawl(URL, depth, maxDepth, visited):
    if depth == maxDepth:
        return

    try:
        html = requests.get(URL)

    except Exception:
        return

    parsed = urlparse(URL)
    protocol = parsed.scheme
    website = parsed.netloc

    soup = BeautifulSoup(html.text, 'html.parser')

    toCheck = list()

    for href in soup.find_all('a', href=True):
        parseLink = urlparse(href['href'])
        toCheck.append(parseLink)

    urlList = list()
    newURL = ''

    for brokeLink in toCheck:
        if brokeLink.path != '':
            if brokeLink.scheme != '' and brokeLink.scheme != 'http' and brokeLink.scheme != 'https':
                pass
            elif brokeLink.scheme == '':
                newURL = protocol + "://"
            else:
                newURL = brokeLink.scheme + "://"
            if brokeLink.netloc == '':
                newURL = newURL + website
            else:
                newURL = newURL + brokeLink.netloc
            newURL = newURL + brokeLink.path
        urlList.append(newURL)

    for link in urlList:
        if link in visited:
            pass
        else:
            print('    ' * depth + link)
            visited.append(link)
            crawl(link, depth+1, maxDepth, visited)
