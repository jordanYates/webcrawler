import sys
from urllib.parse import urlparse
from crawl import crawl

# Make sure URL is passed in
if len(sys.argv) < 2:
    print("Error: no URL supplied")
    exit()

# Read in URL
URL = sys.argv[1]
parsed = urlparse(URL)

# Make sure URL is a full URL
if(parsed.scheme == ''):
    print("Error: invalid URL supplied")
    exit()

if(parsed.netloc == ''):
    print("Error: invalid URL supplied")
    exit()

# Set max depth
if len(sys.argv) >= 3:
    if(int(sys.argv[2])) > 0:
        maxDepth = int(sys.argv[2])
    else:
        maxDepth = 3
else:
    maxDepth = 3

# Initialize a list of visited URLs
visited = list()
visited.append(URL)

crawl(URL, 0, maxDepth, visited)

